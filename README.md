# Notas
## Comenzando un proyecto GIT basics
VCS: Versión Control System
## Para configurar el servidor.
npm -v
npm init
### Configuración de lite-server
https://github.com/johnpapa/lite-server
Si tiene error al ejecutar **npm run dev** usar el comando **npx lite-server**
### Bootstrap
npm install bootstrap --save
### Iconos
npm install open-iconic --save
### SASS en el proyecto
npm install sass --save
### SASS global con node
npm install node-sass --save-dev
### Para ejecutar SASS
npm run scss 
### Para instalar LESS
npm install -g less
### Extensión .less
### Para compilar
lessc css/style.less css/style.css
### onchange y rimraf
npm install --save-dev onchange rimraf
### Concurrently
npm install --save concurrently
### Copyfiles
npm install --save-dev copyfiles
### Minificar Imagen
npm install --save-dev imagemin imagemin-cli
### Minificar CSS HTML JS
npm install --save-dev usemin-cli cssmin uglify htmlmin
### Grunt
npm install grunt --save-dev
npm install grunt-contrib-sass --save-dev
npm install grunt-contrib-watch --save-dev
npm install grunt-browser-sync --save-dev
npm install grunt-contrib-imagemin #### Imagemin para grunt.
npm install time-grunt --save-dev
npm install jit-grunt --save-dev
npm install grunt-contrib-copy
npm install grunt-filerev --save-dev
npm install grunt-contrib-uglify --save-dev
npm install grunt-contrib-cssmin --save-dev
npm install grunt-contrib-concat --save-dev
npm install grunt-contrib-clean --save-dev
npm install grunt-contrib-copy --save-dev
### Gulp
npm install -g gulp-cli
npm install gulp --save-dev
npm install gulp-sass --save-dev
### Crear archivo gulpfile.js
### Instalar todo lo que instalamos en Grunt pero para Gulp.
